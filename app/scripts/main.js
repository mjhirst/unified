(function() {
    'use strict';
    var pdfLinks = $('.pdfs a');
    var thankYou = $.cookie('Thanks');
    var userDownload = $.cookie('User');
    var survivalDownload = $.cookie('Survival');
    var hideThanks = $.cookie('HideThanks');
    var currentDomain = document.domain;
    var honey = document.getElementById('emailAddressBiz');
    // Create Cookie Function
    $(document).ready(function() {
        if ($('.parsley-form').length) {
            $('.parsley-form').parsley().subscribe('parsley:form:validate', function(formInstance) {
                if (formInstance.isValid('block1', true) && honey.value === 'yellow') {
                    $.cookie('User', 'Download', {expires: 365, path: '/'});
                    $.cookie('Thanks', 'Message', {expires: 365, path: '/'});
                    return;
                } else {
                    formInstance.submitEvent.preventDefault();
                    console.log('stopped form submit');
                }
            });
        }
        if ($('.parsley-form-survival').length) {
            $('.parsley-form-survival').parsley().subscribe('parsley:form:validate', function(formInstance) {
                if (formInstance.isValid('block1', true)) {
                    $.cookie('User', 'Download', {expires: 365, path: '/'});
                    $.cookie('Survival', 'Download', {expires: 365, path: '/'});
                } else {
                    formInstance.submitEvent.preventDefault();
                }
            });
        }
        if ($('.parsley-form-guarantee').length) {
            $('.parsley-form-guarantee').parsley().subscribe('parsley:form:validate', function(formInstance) {
                if (formInstance.isValid('block1', true)) {
                    $.cookie('User', 'Download', {expires: 365, path: '/'});
                    $.cookie('Thanks', 'Message', {expires: 365, path: '/'});
                    return;
                } else {
                    formInstance.submitEvent.preventDefault();
                }
            });
        }
        if ($('.parsley-form-downloads').length) {
            $('.parsley-form-downloads').parsley().subscribe('parsley:form:validate', function(formInstance) {
                if (formInstance.isValid('block1', true)) {
                    $.cookie('User', 'Download', {expires: 365, path: '/'});
                    $.cookie('Thanks', 'Message', {expires: 365, path: '/'});
                    return;
                } else {
                    formInstance.submitEvent.preventDefault();
                }
            });
        }
    });
    if (userDownload === undefined) {
        for (var i = 0; i < pdfLinks.length; i++) {
            // Remove href from links
            pdfLinks[i].removeAttribute('href');
        }
    }
    if (userDownload === 'Download') {
        for (var funk = 0; funk < pdfLinks.length; funk++) {
            // Remove attributes required for modal
            pdfLinks[funk].removeAttribute('data-target');
            pdfLinks[funk].removeAttribute('data-toggle');
        }
    }
    if (survivalDownload === 'Download') {
        $('.adweek-promo-banner').hide();
        $('.thanks-survival-download').show();
    }
    if (thankYou === 'Message') {
        $('.thanks-submit').show().delay(2500).fadeOut(1500);
        $.cookie('HideThanks', 'True', {
            expires: 365,
            path: '/',
            domain: currentDomain
        });
    }
    if (hideThanks === 'True') {
        $('.thanks-submit').hide();
    }
    (function() {
        var closeNav, navList, toggleNav, contactLink;

        closeNav = $('#close-nav');

        navList = $('.nav-list');

        toggleNav = $('.navbar-toggle');

        contactLink = $('.contact-link');

        toggleNav.on('click', function() {
            return navList.toggle();
        });

        closeNav.on('click', function() {
            return navList.toggle();
        });

        contactLink.on('click', function(){
            return navList.toggle();
        });

    }).call(this);
})();